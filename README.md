ASP.NET Core Web API project of Unit Converter  

Database can be added by below two commands

In the Package Manager Console window, Run the following commands:

1. Add-Migration Initial 
2. Update-Database

UnitConverter application has three CURD opertions:

1. GetAll which returns Metric units to Imperial units and vice versa data in json form
2. Convert which convert value from one unit  to other unit and return string 
3. Create(Post) which add the new conversion in database 


Also, added 3 Xunits test cases  of convert, getAll and post in  UnitConverter.Test application

