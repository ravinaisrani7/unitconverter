﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitConverter.Test.MockData
{
    public class UnitConverterMockData
    {
        public static List<UnitConverter> GetUnitConverter(string from,string to,decimal value)
        {
            return new List<UnitConverter>{
             new UnitConverter{
                 Id = 1,
                 From = "inch",
                 To = "centimeters",
                 Value = (decimal)2.54
             },
             new UnitConverter{
                      Id = 2,
                 From = "foot",
                 To = "meters",
                 Value =(decimal) 0.3048
             },
             new UnitConverter{
                 Id = 3,
                     
                 From = "mile",
                 To = "kilometers",
                 Value = (decimal)1.6093
             }
         };
        }

        public static List<UnitConverter> GetUnitConverter()
        {
            return new List<UnitConverter>();
        }

        public static UnitConverter NewTodo()
        {
            return new UnitConverter
            {
                Id = 0,
                From = "millimeter",
                To = "inch",
                Value = (decimal)0.3094
            };
        }
    }
}

