﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitConverter.Models;
using UnitConverter.Services;
using UnitConverter.Test.MockData;
using Xunit;



namespace UnitConverter.Test.Services
{
    public class TestUnitConverterService:IDisposable
{
    protected readonly UnitConverterContext _context;
        public TestUnitConverterService()
        {
            var options = new DbContextOptionsBuilder<UnitConverterContext>()
            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
            .Options;

            _context = new UnitConverterContext(options);

            _context.Database.EnsureCreated();
        }

        [Fact]
        public async Task GetAllAsync_ReturnTodoCollection()
        {
            /// Arrange
            _context.UnitConverterMultilpyValue.AddRange(MockData.UnitConverterMockData.GetUnitConverter());
            _context.SaveChanges();

            var sut = new UnitConverterService(_context);

            /// Act
            var result = await sut.Get(2, "inch", "centimeters");

            /// Assert
            //result.sho().HaveCount(UnitConverterMockData.GetUnitConverter().Count);
        }

        [Fact]
        public async Task SaveAsync_AddNewTodo()
        {
            /// Arrange
            var newUnitConverter = UnitConverterMockData.NewTodo();
            _context.UnitConverterMultilpyValue.AddRange(MockData.UnitConverterMockData.GetUnitConverter());
            _context.SaveChanges();

            var sut = new UnitConverterService(_context);

            /// Act
            await sut.Post(newUnitConverter);

            ///Assert
            int expectedRecordCount = (UnitConverterMockData.GetUnitConverter().Count() + 1);
            //_context.UnitConverterMultilpyValue.Count().Should().Be(expectedRecordCount);
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
    }
}
