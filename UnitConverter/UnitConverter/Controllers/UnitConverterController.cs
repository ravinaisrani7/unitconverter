﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UnitConverter.Models;
using UnitConverter.Services;

namespace UnitConverter.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class UnitConverterController : ControllerBase
    {
        private readonly IUnitConverterService _unitConverterService;
 
        public UnitConverterController(IUnitConverterService unitConverterService)
        {
            _unitConverterService = unitConverterService;
        }
        [HttpGet]
        public async Task<ActionResult<string>> Convert(string value,string from,string To)
        {
       var result=     _unitConverterService.Convert( value,  from,  To);
            
               return result.Result;
        



        }
    
    [HttpGet]
    public async Task<IActionResult> GetAll()
    {
        var result = await _unitConverterService.GetAll();
        if (result.Count == 0)
        {
            return NoContent();
        }
        return Ok(result);



    }
    [HttpPost]
        public async Task<IActionResult> Post(UnitConverter unitConverter)
        {
           
            await _unitConverterService.Post(unitConverter);
            return Ok();
           

        }

     
    }
}
