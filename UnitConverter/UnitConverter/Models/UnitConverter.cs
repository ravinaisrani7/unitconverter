﻿using System.ComponentModel.DataAnnotations.Schema;

namespace UnitConverter
{
    public class UnitConverter
    {
        public int Id { get; set; }
        public string From { get; set; }

        public string To { get; set; }
     
        //public decimal Value { get; set; }

        public string Value { get; set; }
    }
}
