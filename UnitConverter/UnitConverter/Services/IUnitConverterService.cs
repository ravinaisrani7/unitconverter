﻿using Microsoft.AspNetCore.Mvc;

namespace UnitConverter.Services
{
    public interface IUnitConverterService
    {


        Task<ActionResult<string>> Convert(string value, string from, string To);

        Task<List<UnitConverter>> GetAll();

        Task Post(UnitConverter unitConverter);

   

    }
}
