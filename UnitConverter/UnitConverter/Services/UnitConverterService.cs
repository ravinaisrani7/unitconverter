﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using UnitConverter.Models;

namespace UnitConverter.Services
{
    public class UnitConverterService : IUnitConverterService
    {
        private readonly UnitConverterContext _dbContext;
   
        public UnitConverterService(UnitConverterContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ActionResult<string>> Convert(string value, string from, string To)
        {
            IList<UnitConverter> lstUnitConverter=  _dbContext.UnitConverterMultilpyValue.ToList();
            var unitConverter = lstUnitConverter.Where(a => a.From == from && a.To == To).FirstOrDefault();

            
            if (unitConverter == null)
            {
                return "Invalid input ";
            }
            else
            {
                decimal multiplyByValue;decimal valueTobeConvert, convetedValue; 
                   
                decimal.TryParse(unitConverter.Value, out multiplyByValue);
                if(multiplyByValue == 0)
                {
                    
                   string val= Regex.Replace(unitConverter.Value, "[A-Z]", value);
                    string calculatedValue = new DataTable().Compute(val, null).ToString();
                    decimal.TryParse(calculatedValue, out convetedValue);


                }
                else
                {
                    decimal.TryParse(value, out valueTobeConvert);
                   convetedValue = multiplyByValue * valueTobeConvert;
                }
            
                
                return value + " " + from + " =  " + convetedValue + " " + " " + To;
            }


        }

        public async Task<List<UnitConverter> > GetAll()
        {
            return await _dbContext.UnitConverterMultilpyValue.ToListAsync();
        }

        public async Task Post(UnitConverter unitConverter)
        {
           _dbContext.UnitConverterMultilpyValue.Add(unitConverter);
            await _dbContext.SaveChangesAsync();
        }

       

    }
}
